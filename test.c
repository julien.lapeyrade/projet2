#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

int verifier_format(char *ip);

int main(){
char *buffer = NULL;
size_t size_buffer = 0;
int size_char = 0;
printf("Veuillez entrer l'adresse IP et le masque sous la forme: x.x.x.x/masque  :\n");
size_char = getline(&buffer, &size_buffer,stdin);
int verif = verifier_format(buffer);
if(verif == 0){
    char *erreur    = "";

    printf("Le format n'est pas respecté\n");
    exit(0);

 }
}

int verifier_format(char *ip){
   int err;
   regex_t preg;
   //char str_request[] = ip;
   const char *str_regex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
   err = regcomp (&preg, str_regex, REG_NOSUB | REG_EXTENDED);

   if (err == 0){
      int match;
      match = regexec (&preg, ip, 0, NULL, 0);
      if(match == 0){
         return 1;
      }
      else{
        return 0;
      }
   }
}


