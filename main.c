#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <math.h>



int verification(char *b);
void classe(int* Tip);
void mettreEnTableau(char *b, int size_char, int* result);


int main(){

char *buffer = NULL;
size_t size_buffer = 0;
int size_char = 0;
int Tip[5] = {0};
printf("Veuillez entrer l'adresse IP et le masque sous la forme: x.x.x.x/masque\n");
size_char = getline(&buffer, &size_buffer,stdin);
int verif = verification(buffer);

if(verif == 0){
    char *erreur = "";

    printf("format non respecté\n");
    exit(0);
 
 }
 /* Pour l'affichage */
 printf("\n          *********************************\n          *                               *\n          *          ADRESSE IP           *\n          *                               *\n          *********************************\n");
 
mettreEnTableau(buffer, size_char, &Tip);
classe(&Tip);


}



void mettreEnTableau(char *b, int size_char, int* result) {

    char ip_tableau[size_char];
    for(int r = 0; r < size_char; r++){
        ip_tableau[r] = *b;
        b++;
    }

    char temp[3] = {"aaa"};
    int z = 0;
    int x = 0;
    for(int i = 0; i < size_char; i++){
        if(ip_tableau[i] == '.'){
            result[x] = strtol( temp, NULL, 10 );
            x++;
            z=0;
            temp[0] = 'a';
            temp[1] = 'a';
            temp[2] = 'a';
        }
        else if (ip_tableau[i] == '/'){
            result[x] = strtol( temp, NULL, 10 );
            x++;
            z=0;
            temp[0] = 'a';
            temp[1] = 'a';
            temp[2] = 'a';
        }
        else if(ip_tableau[i] == '\n')
        {
            result[x] = strtol( temp, NULL, 10 );
            x++;
            z=0;
            temp[0] = 'a';
            temp[1] = 'a';
            temp[2] = 'a';
        }
        else {
            temp[z] = ip_tableau[i];
            z++;
        }
    }

        
        
      



}


 int verification (char *b) { 

        int err;
        regex_t preg;

        // Permet de creer une expression reguliere qui permet de verifier tout les types d'adressses 

        const char *str_regex = "[[:digit:]]{1,3}\\.[[:digit:]]{1,3}\\.[[:digit:]]{1,3}\\.[[:digit:]]{1,3}\\/[[:digit:]]{1,2}"; //ENCORE DES TESTS À FAIRE AVEC L'EXPRESSION REGULIERE.
        err = regcomp (&preg, str_regex, REG_NOSUB | REG_EXTENDED);

        if (err == 0) {

            int match;
            match = regexec (&preg, b, 0, NULL, 0);

            if (match == 0) { //Si la chaine est valide alors la variable match vaut 0 et le sous programme verification vaut 0.

                
                return 1;

            } else {

            
            return 0;

            }

        }

    }

//----------------------------------------------------------------------------------------------------------------------------------------------------



void classe(int* Tip){

u_int32_t sum = (Tip[0]<<24)  + (Tip[1]<<16) + (Tip[2]<<8) + Tip[3];

u_int32_t mask = (0xFFFFFFFF << (32 - Tip[4])) & 0xFFFFFFFF;
u_int32_t network = sum & mask;

u_int8_t d = network & 0xFF;
u_int8_t c = (network >> 8) & 0xFF;
u_int8_t b = (network >> 16) & 0xFF;
u_int8_t a = (network >> 24) & 0xFF;   

  /* On cherche la classe */ 
  if (Tip[0]>0 && Tip[0]<128){
    printf("\nCLASSE A\n");
    printf("\nL'adresse réseau est:  %d.%d.%d.%d\n",a,b,c,d);
  }

  else if (Tip[0]>127 && Tip[0]<192){
    printf("\nCLASSE B\n");
    printf("\nL'adresse réseau est:  %d.%d.%d.%d\n",a,b,c,d);
  }

  else if (Tip[0]>191 && Tip[0]<224){
    printf("\nCLASSE C\n");
    printf("\nL'adresse réseau est:  %d.%d.%d.%d\n",a,b,c,d);
  }

  else if (Tip[0]>223 && Tip[0]<240){
    printf("\nCLASSE D\n");
    printf("\nL'adresse réseau est:  %d.%d.%d.%d\n",a,b,c,d);
  }

  else if (Tip[0]>239 && Tip[0]<256){
    printf("\nCLASSE E\n");
    printf("\nL'adresse réseau est:  %d.%d.%d.%d\n",a,b,c,d);
  }


  else printf("\nadresse non valide\n");

/* On cherche si l'adresse est privée sinon elle est public. Toujours avec le nombre decimal*/ 
  if (Tip[0]==10||Tip[0]==172 && Tip[1] > 15 && Tip[1]<31||Tip[0]==192 && Tip[1]==168){
    printf("\nL'adresse est Privée\n");
  }else {
    printf("\nL'adresse est Public\n");
  }

  if (Tip[0]==255&& Tip[1]==255 && Tip[2]==255 && Tip[3]==255){
    printf("\nC'est une adresse Brodcast\n");
  }
  if (Tip[0]==127 && Tip[1]==0 && Tip[2]==0 && Tip[3]==1){
    printf("\nC'est une adresse Localhost\n");
  }
  if (Tip[0]>223 && Tip[0]<240){
    printf("\nC'est une adresse Multicast\n");
  }

   if((sum & mask)==sum){
     printf("\nL'adresse de l'hote est:  %d.%d.%d.%d\n",Tip[0],Tip[1],Tip[2],Tip[3]);
   }

/* Pour l'affichage */
printf("\n ");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------



