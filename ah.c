#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int verifier_format(char *ip)
{
   int err;
   regex_t preg;
   //char str_request[] = ip;
   const char *str_regex = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\/[0-9]{1,2}";
   err = regcomp (&preg, str_regex, REG_NOSUB | REG_EXTENDED);
   if (err == 0)
   {
      int match;


      match = regexec (&preg, ip, 0, NULL, 0);
      if(match == 0)
      {
         return 1;
      }
      else
      {
        return 0;
      }
   }
}