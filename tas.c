#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

    // Cela rendra aussi le code source plus lisible
#define ARRET 0
#define MARCHE 1
void main(){
// Pour améliorer encore la lisibilité :
typedef unsigned char OCTET; // définition du type OCTET (8 bits)
OCTET fabriquerCommandes8TOR(OCTET commandes8TORPrecedentes, int numeroSortie,
int action)
{
OCTET commandes8TOR, masque;
masque = (1 << numeroSortie);
if(action == MARCHE)
commandes8TOR = commandes8TORPrecedentes | masque;
else commandes8TOR = commandes8TORPrecedentes & ~masque;
return commandes8TOR;
}
printf("%d", &masque );
    }